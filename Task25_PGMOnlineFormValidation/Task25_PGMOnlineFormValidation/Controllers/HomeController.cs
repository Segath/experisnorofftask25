﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task25_PGMOnlineFormValidation.Models;

namespace Task25_PGMOnlineFormValidation.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult SupervisorForm()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorForm(Supervisor sup)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Add(sup);
                return Redirect("SupervisorInfo");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View(SupervisorGroup.Supervisors);
        }

        [HttpGet]
        public IActionResult SupervisorTestList()
        {
            List<Supervisor> supervisorInfo = new List<Supervisor>();

            foreach (Supervisor s in Supervisor.CreateSupervisorTestList())
            {
                int? id = s?.Id ?? -2;
                string name = s?.Name ?? "No name. null object";
                string subject = s?.Subject ?? "No subject. null object";
                supervisorInfo.Add(new Supervisor() { Id = id, Name = name, Subject = subject });
            }

            return View(supervisorInfo);
        }

        [HttpGet]
        public IActionResult SupervisorTestListFirstLetter()
        {
            return View(Supervisor.CreateSupervisorTestList().Where(s => s?.Name[0] == 'S'));
        }
    }
}
