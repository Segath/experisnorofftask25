﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task25_PGMOnlineFormValidation.Models
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> supervisorGroup = new List<Supervisor>();

        public static List<Supervisor> Supervisors
        {
            get { return supervisorGroup; }
        }

        public static void Add(Supervisor sup)
        {
            supervisorGroup.Add(sup);
        }

    }
}
