﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task25_PGMOnlineFormValidation.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "Please enter an Id")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid whole number")]
        public int? Id { get; set; } = -1;

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "Nameless, uninstantiated";

        [Required(ErrorMessage = "Please enter a subject")]
        public string Subject { get; set; } = "None, uninstantiated";

        public static List<Supervisor> CreateSupervisorTestList()
        {
            List<Supervisor> supL = new List<Supervisor>
            {
                new Supervisor() {Id = 10, Name = "Sally", Subject = "Operating Systems"},
                new Supervisor() {Id = 11, Name = "Bob", Subject = "Web Programming"},
                new Supervisor(),
                null
            };
            return supL;
        }
    }
}
